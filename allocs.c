#include "allocs.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>

struct record* alloc_heap(size_t sz) {
  return (struct record*)malloc(sz * sizeof (struct record));
}

struct record* alloc_mmap(size_t sz) {
  void *addr = mmap( 0
                   , sz * sizeof (struct record)
                   , PROT_READ|PROT_WRITE
                   , MAP_PRIVATE|MAP_ANONYMOUS
                   , 0
                   , 0);
  if (addr == MAP_FAILED) {
    puts("can't mmap");
    exit(-1);
  }
  return (struct record*)addr;
}

struct record* alloc_mmap_huge(size_t sz) {
  void *addr = mmap( 0
                   , sz * sizeof (struct record)
                   , PROT_READ|PROT_WRITE
                   , MAP_PRIVATE|MAP_ANONYMOUS|MAP_HUGETLB
                   , 0
                   , 0);
  if (addr == MAP_FAILED) {
    puts("can't mmap");
    exit(-1);
  }
  return (struct record*)addr;
}
