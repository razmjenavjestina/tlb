#ifndef ALLOCS_H_
#define ALLOCS_H_

#include <stdlib.h>
#include "record.h"

struct record* alloc_heap(size_t sz);
struct record* alloc_mmap(size_t sz);
struct record* alloc_mmap_huge(size_t sz);

#endif //ALLOCS_H_
