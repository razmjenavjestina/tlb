#include "stopwatch.h"

#include <stdlib.h>

struct timeval* stopwatch_start() {
  struct timeval *tv = malloc(sizeof (struct timeval));
  gettimeofday(tv, NULL);
  return tv;
}

double stopwatch_stop(struct timeval *from) {
  struct timeval to;
  gettimeofday(&to, NULL);
  double dt = (to.tv_sec - from->tv_sec) * 1000.0;
  dt += (to.tv_usec - from->tv_usec) / 1000.0;
  free(from);
  return dt;
}
