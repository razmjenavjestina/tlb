all: test1 generate

test1: main1.c stopwatch.c stopwatch.h record.h allocs.c allocs.h
	gcc -o test1 --std=gnu11 -Wall -O3 main1.c stopwatch.c allocs.c

generate: gen.c record.h
	gcc -o generate --std=gnu11 -Wall -O3 gen.c

hugetlb:
	sudo sh -c "echo 3000 > /proc/sys/vm/nr_hugepages"

clean:
	rm -rf *.o test1 generate dump.bin
