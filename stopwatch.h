#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <sys/time.h>

struct timeval* stopwatch_start();
double stopwatch_stop(struct timeval *from);

#endif //STOPWATCH_H_
