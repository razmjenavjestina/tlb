#ifndef RECORD_H_
#define RECORD_H_

#include <stdint.h>

struct record {
  uint32_t id;
  uint32_t t;
  uint8_t buff[20];
};

#endif //RECORD_H_
