#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <memory.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "consts.h"
#include "record.h"
#include "stopwatch.h"
#include "allocs.h"

static struct record* mmap_file(const char *path, size_t sz) {
  int fd = open(path, O_RDONLY);
  void *addr = mmap( 0
                   , sz * sizeof (struct record)
                   , PROT_READ
                   , MAP_PRIVATE
                   , fd
                   , 0);
  if (addr == MAP_FAILED) puts("can't mmap");
  return (struct record*)addr;
}

static int* cummulative_sums_by_id( struct record *ptr
                                  , size_t sz
                                  , unsigned maxid ) {
  int *sums    = calloc(maxid, sizeof(int));
  int *cumsums = malloc((maxid + 1) * sizeof(int));
  int i;

  for (i = 0; i < sz; ++i) ++sums[(ptr + i)->id];
  cumsums[0] = 0;
  for (i = 0; i < maxid; ++i) cumsums[i + 1] = cumsums[i] + sums[i];
  free(sums);
  return cumsums;
}

static struct record* group( const struct record *in
                           , size_t sz
                           , int *cumsums
                           , unsigned maxid ) {
  int *progress = calloc(maxid, sizeof(int));
  struct record *out = alloc_mmap_huge(sz);
  int i;
  for (i = 0; i < sz; ++i) {
    uint32_t id = in[i].id;
    memcpy(out + cumsums[id] + progress[id], in + i, sizeof (struct record));
    ++progress[id];
  }
  free(progress);
  return out;
}

struct uniq {
  uint32_t last;
  int count;
};

static inline void uniqfn(const struct record *ptr, void *data) {
  uint32_t id = ptr->id;
  struct uniq *u = (struct uniq*)data;
  if (u->last != id) {
    u->last = id;
    ++u->count;
  }
}

static inline void sumfn(const struct record *ptr, void *data) {
  long long *sum = (long long*)data;
  *sum += ptr->t;
}

static void reduce( const struct record *ptr
                  , size_t sz
                  , void(*fn)(const struct record*, void*)
                  , void *data) {
  int i;
  for (i = 0; i < sz; ++i) fn(ptr + i, data);
}

static void reducers( struct record *in
                    , struct record *out
                    , size_t sz) {
  struct timeval *sw;
  long long sum;
  sw = stopwatch_start();
  sum = 0; reduce(in, sz, sumfn, &sum);
  printf("in : %lld\n", sum);

  sum = 0; reduce(out, sz, sumfn, &sum);
  printf("out: %lld\n", sum);

  struct uniq u1 = { .last = (uint32_t)-1, .count=0 };
  reduce(in, sz, uniqfn, &u1);
  printf("uniq broken: %d\n", u1.count);

  struct uniq u2 = { .last = (uint32_t)-1, .count=0 };
  reduce(out, sz, uniqfn, &u2);
  printf("uniq: %d\n", u2.count);

  printf("reducers: %lf\n", stopwatch_stop(sw));
}


int main() {
  struct record *in, *out;
  int* cumsums;
  struct timeval *sw;

  in = mmap_file("dump.bin", XXSIZE);
  sw = stopwatch_start();
  cumsums = cummulative_sums_by_id(in, XXSIZE, XXMAXID);
  printf("cummulative sums: %lf\n", stopwatch_stop(sw));

  sw = stopwatch_start();
  out = group(in, XXSIZE, cumsums, XXMAXID);
  printf("group: %lf\n", stopwatch_stop(sw));

  reducers(in, out, XXSIZE);
  return 0;
}
