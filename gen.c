#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "consts.h"
#include "record.h"

static inline void zero_buff(uint8_t *ptr, size_t sz) {
  int i;
  for (i = 0; i < sz; ++i) ptr[i] = 0;
}

static inline void record_make(FILE *fp, unsigned maxid) {
  static uint32_t t = 0;
  struct record rec;
  rec.id = rand() % maxid;
  rec.t  = t++;
  zero_buff(rec.buff, 20);
  fwrite(&rec, sizeof rec, 1, fp);
}

static void records_dump(size_t sz, unsigned maxid) {
  FILE *fp = fopen("dump.bin", "w");
  int i;

  for (i = 0; i < sz; ++i) record_make(fp, maxid);
  fclose(fp);
}

int main() {
  srand(1);
  records_dump(XXSIZE, XXMAXID);
}
